<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $dataContent = Category::whereDeletedAt(null)->orderBy('name');
        $dataContent = $this->withFilter($dataContent, $request);
        $dataContent = $dataContent->paginate(25);
        return $dataContent;
    }

    public function dataList()
    {
        return Category::orderBy('name')->get();
    }

    public function store(Request $request)
    {
        $this->validateData($request);

        if ($request->image){
            $name = $this->imageProcessing($request->image);
            $request->merge(['image' => $name]);
        }

        Category::create($request->all());
    }

    public function update(Request $request, $id)
    {
        if ($request->image && strlen($request->image) > 100 ){
            $name = $this->imageProcessing($request->image);
            $request->merge(['image' => $name]);
        }

        Category::find($id)->update($request->all());
    }

    public function destroy($id)
    {
        Category::findOrFail($id)->update([
            'deleted_at'    => now()
        ]);
    }

    public function withFilter($dataContent, $request){
        if ($request->name != null){
            $dataContent = $dataContent->where('name','LIKE', '%'.$request->name.'%');
        }

        if ($request->desc != null){
            $dataContent = $dataContent->where('desc','LIKE', '%'.$request->desc.'%');
        }
        return $dataContent;
    }

    public function validateData($request){
        $this->validate($request, [
            "name"          => 'required',
            "slug"          => 'required',
        ]);
    }
}
