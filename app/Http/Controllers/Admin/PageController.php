<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $dataContent = Page::orderByDesc('created_at');
        $dataContent = $this->withFilter($dataContent, $request);
        $dataContent = $dataContent->paginate(25);
        return $dataContent;
    }

    public function store(Request $request)
    {
        $this->validateData($request);

        if ($request->image){
            $name = $this->imageProcessing($request->image);
            $request->merge(['image' => $name]);
        }

        Page::create($request->all());

        return 'success';
    }

    public function show($id){
        return Page::find($id);
    }

    public function update(Request $request, $id)
    {
        if ($request->image && strlen($request->image) > 100 ){
            $name = $this->imageProcessing($request->image);
            $request->merge(['image' => $name]);
        }

        Page::find($id)->update($request->all());
    }

    public function destroy($id)
    {
        Page::findOrFail($id)->delete();
    }

    public function withFilter($dataContent, $request){
        if ($request->title != null){
            $dataContent = $dataContent->where('title','LIKE', '%'.$request->title.'%');
        }

        if ($request->status != null){
            $dataContent = $dataContent->whereStatus($request->status);
        }
        return $dataContent;
    }

    public function imageProcessing($img){
        $name = 'page/'.date('Y-m').'/'.time().'.' . explode('/', explode(':', substr($img, 0, strpos($img, ';')))[1])[1];

        $base64_image = $img;
        @list($type, $file_data) = explode(';', $base64_image);
        @list(, $file_data) = explode(',', $file_data);

        Storage::disk('public')->put($name, base64_decode($file_data));

        return $name;
    }

    public function imageUpload(Request $request){
        $image = $request->image;
        $name = 'page/'.date('Y-m').'/'.time().'.'.$image->getClientOriginalExtension();
        $img = Image::make($_FILES['image']['tmp_name']);
        $img->fit(300, null);
        $img->stream();
        Storage::disk('public')->put($name, $img, 'public');

        return response()->json(['url'=> '/assets/'.$name]);
    }

    public function validateData($request){
        $this->validate($request, [
            "title"          => 'required',
            "body"          => 'required',
        ],[
//            'name.required'     => 'Nama harus diisi',
//            'address.required'  => 'Alamat harus diisi',
        ]);
    }
}
