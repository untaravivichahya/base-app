<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('admin.layout');
    }

    public function dashboardCard(){
        $data['posts'] = Post::count();
        $data['pages'] = Page::count();
        $data['users'] = User::count();
        $data['comments'] = 0;

        return $data;
    }
}
