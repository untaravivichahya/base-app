<?php

//Website
Route::get('/', 'Web\PageController@home');
Route::get('/blogs', 'Web\PageController@blogs');

//Admin area
Auth::routes();
Route::get('/cms/{path}', 'Admin\DashboardController@index')->where( 'path' , '([A-z\d\-\/_.]+)?' )->middleware('auth');
Route::group(['prefix' => 'cms', 'middleware'=>'auth'], function (){
    Route::get('/', function (){return redirect('/cms/dashboard');});
    Route::get('/dashboard', 'Admin\DashboardController@index');
});

//Json
Route::group(['prefix' => 'admin', 'middleware'=>'auth'], function (){
    Route::resource('posts', 'Admin\PostController');
    Route::resource('pages', 'Admin\PageController');
    Route::resource('categories', 'Admin\CategoryController');

    Route::get('get-categories', 'Admin\CategoryController@dataList');
    Route::get('get-dashboard-card', 'Admin\DashboardController@dashboardCard');

    Route::post('post-image-upload', 'Admin\PostController@imageUpload');
    Route::post('page-image-upload', 'Admin\PostController@imageUpload');
});
