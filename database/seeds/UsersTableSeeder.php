<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'role' => 'admin',
                'password' => Hash::make('admin@admin.com'),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'client',
                'email' => 'client@client.com',
                'role' => 'client',
                'password' => Hash::make('client@client.com'),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];
        DB::table('users')->insert($users);
    }
}
