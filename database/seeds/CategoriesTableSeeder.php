<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'          => 'slider',
                'slug'          => 'slider',
                'desc'          => 'Muncul pada slider home',
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
            [
                'name'          => 'news',
                'slug'          => 'news',
                'desc'          => 'Muncul pada section News',
                'created_at'    => now(),
                'updated_at'    => now(),
            ]
        ];
        DB::table('categories')->insert($categories);
    }
}
