<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <router-link to="/cms/dashboard" class="nav-link">
                <i class="nav-icon fa fa-tachometer"></i>
                <p>
                    Dashboard
                </p>
            </router-link>
        </li>
        <li class="nav-item">
            <router-link to="/cms/post" class="nav-link">
                <i class="nav-icon fa fa-paper-plane"></i>
                <p>
                    Posts
                </p>
            </router-link>
        </li>
        <li class="nav-item">
            <router-link to="/cms/page" class="nav-link">
                <i class="nav-icon fa fa-align-left"></i>
                <p>
                    Pages
                </p>
            </router-link>
        </li>
        <li class="nav-item">
            <router-link to="/cms/category" class="nav-link">
                <i class="nav-icon fa fa-paperclip"></i>
                <p>
                    Category
                </p>
            </router-link>
        </li>
        <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off nav-icon danger"></i>
                <p>Logout</p>
            </a>
            <form action="{{route('logout')}}" id="logout-form" method="POST" style="display: none">
                @csrf
            </form>
        </li>
    </ul>
</nav>
