/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap-web');

window.Vue = require('vue');

// VUE ROUTER
import VueRouter from 'vue-router'
Vue.use(VueRouter);
const routes = [
    { path: '/cms/dashboard', component: require('./components/dashboard/Index.vue').default },

];
const router = new VueRouter({
    mode : 'history',
    routes
});

// //---- VForm ----
// import { Form, HasError, AlertError } from 'vform'
// window.form = Form;
// Vue.component(HasError.name, HasError);
// Vue.component(AlertError.name, AlertError);
//
// //---- PROGRESS BAR ----
// import VueProgressBar from 'vue-progressbar'
// Vue.use(VueProgressBar, {
//     color: 'rgb(255,28,25)',
//     failedColor: 'red',
//     height: '20px'
// });
//
// //---- SWEET ALERT ----
// import Swal from 'sweetalert2'
// window.Swal = Swal;
//
// //---- Fire Global Event ----
// window.Fire = new Vue();
//
// // ---- SELECT OPTIONS ----
// import Multiselect from 'vue-multiselect'
// Vue.component('multiselect', Multiselect);
//
// //---- FILTER ----
// require('./vue-filter');
// require('./vue-components');

const app = new Vue({
    el: '#app',
    router,
});
