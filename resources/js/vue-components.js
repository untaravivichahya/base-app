
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.mixin({
    data: function (){
        return {
            hello : 'Hello World!'
        }
    },
    methods: {
        globalPopSwal(){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Success',
                toast: true,
                showConfirmButton: false,
                timer: 1000,
            })
        }
    },
    created() {

    }
})
