/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// VUE ROUTER
import VueRouter from 'vue-router'
Vue.use(VueRouter);
const routes = [
    { path: '/cms/dashboard', component: require('./components/dashboard/Index.vue').default },

    { path: '/cms/post', component: require('./components/post/Index.vue').default },
    { path: '/cms/post/:id', component: require('./components/post/Show.vue').default },
    { path: '/cms/post/edit-add/:id', component: require('./components/post/EditAdd.vue').default },

    { path: '/cms/page', component: require('./components/page/Index.vue').default },
    { path: '/cms/page/:id', component: require('./components/page/Show.vue').default },
    { path: '/cms/page/edit-add/:id', component: require('./components/page/EditAdd.vue').default },

    { path: '/cms/category', component: require('./components/category/Index.vue').default },
];
const router = new VueRouter({
    mode : 'history',
    routes
});

//---- VForm ----
import { Form, HasError, AlertError } from 'vform'
window.form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

//---- PROGRESS BAR ----
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(20,96,236)',
    failedColor: 'red',
    thickness: '5px'
});

//---- SWEET ALERT ----
import Swal from 'sweetalert2'
window.Swal = Swal;

//---- Fire Global Event ----
window.Fire = new Vue();

// ---- SELECT OPTIONS ----
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect);

//---- FILTER ----
require('./vue-filter');
require('./vue-components');

const app = new Vue({
    el: '#app',
    router,
});
