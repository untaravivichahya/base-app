const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

// mix
    // .js('resources/js/web.js', 'public/js')
    // .sass('resources/sass/web2.scss', 'public/css');

// mix.scripts([
//     'public/web/js/jquery-2.2.4.min.js',
//     'public/web/js/jquery-ui.min.js',
//     'public/web/js/bootstrap.min.js',
//     'public/web/js/jquery-plugin-collection.js',
//     //Slider
//     'public/web/js/revolution-slider/js/jquery.themepunch.tools.min.js',
//     'public/web/js/revolution-slider/js/jquery.themepunch.revolution.min.js',
//     //
//     'public/web/js/html5shiv.min.js',
//     'public/web/js/respond.min.js',
//     'public/web/js/mixing.js',
//     'public/web/js/custom.js',
//     'resource/js/web.js',
//     'resource/web/js/additional.js',
//     //
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.actions.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.migration.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js',
//     'public/web/js/revolution-slider/js/extensions/revolution.extension.video.min.js',
// ], 'public/js/web2.js');
